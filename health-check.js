let docker = require('./docker');

module.exports = healthCheck;

function healthCheck(image) {
    return (image && async function () {
        const containersRunning = await docker.apiRequest(`containers/json?filters=${JSON.stringify({ status: ['running'] })}`);
        if (!containersRunning.some(container =>
            typeof container.Image === 'string' && (
                container.Image === image ||
                container.Image.startsWith(image + ':')
            ))) {
            throw Error('No matching container running.');
        }
    }) || undefined;
}