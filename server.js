let http = require('http');
let url = require('url');
let metrics = require('./metrics');
let ec2 = require('./ec2');

let config = {
    STATS_EXPORTER_PORT: '9100',
    STATS_EXPORTER_RUNNING_ON_EC2: ec2.detectIfRunningOnEC2().toString(),
    STATS_EXPORTER_LABEL_MAPPINGS:
        'cluster=com.amazonaws.ecs.cluster;' +
        'task=com.amazonaws.ecs.task-definition-family,com.docker.compose.project;' +
        'container=com.amazonaws.ecs.container-name,com.docker.compose.service',
    STATS_EXPORTER_METRIC_PREFIX: 'container_',
    STATS_EXPORTER_SAMPLE_SIZE: '10',
    STATS_EXPORTER_IGNORE_IMAGES: 'amazon/amazon-ecs-agent',
    STATS_EXPORTER_HEALTH_CHECK_IMAGE: 'amazon/amazon-ecs-agent',
    ...process.env
};

let healthCheck = require('./health-check')(config.STATS_EXPORTER_HEALTH_CHECK_IMAGE);

let server = http.createServer((request, response) => {
    
    let { method } = request;
    let { pathname } = url.parse(request.url);

    switch (`${method} ${pathname}`) {
        
        case "GET /metrics":
            response.statusCode = 200;
            response.setHeader('Content-Type', metrics.contentType);
            response.setHeader('Cache-Control', ['no-cache', 'no-store']);
            response.setHeader('Pragma', ['no-cache']);
            response.setHeader('Expires', '-1');
            response.end(metrics.dump());
            break;
        
        case "GET /__healthcheck":
            if (healthCheck) {
                healthCheck()
                    .then(() => [200, 'OK'])
                    .catch(e => [500, 'FAIL: ' + e.message])
                    .then(([statusCode, text]) => {
                        response.statusCode = statusCode;
                        response.setHeader('Content-Type', 'text/plain');
                        response.setHeader('Cache-Control', ['no-cache', 'no-store']);
                        response.setHeader('Pragma', ['no-cache']);
                        response.setHeader('Expires', '-1');
                        response.end(text);
                    });
                break;
            }
        
        default:
            response.statusCode = 404;
            response.end();
            break;
    }
});

Promise.
    resolve(
        (/^\s*(true|1)\s*$/i.test(config.STATS_EXPORTER_RUNNING_ON_EC2)
            ? ec2.getMetaData('/latest/meta-data/instance-id').then(instance => ({ instance }))
            : {})
    ).
    then(labels => {

        let port = parseInt(config.STATS_EXPORTER_PORT);
        
        let labelMappings =
            config.STATS_EXPORTER_LABEL_MAPPINGS.
                split(';').
                map(entry => entry.split('=', 2)).
                reduce((map, { 0: key, 1: values }) => (
                    values
                        ? ({ ...map, [key]: values.split(',').filter(value => value.length > 0) })
                        : map),
                    {}
            );
        
        let prefix = config.STATS_EXPORTER_METRIC_PREFIX;
        let sampleSize = config.STATS_EXPORTER_SAMPLE_SIZE;
        let ignoreImages = config.STATS_EXPORTER_IGNORE_IMAGES.
            split(',').
            map(image => image.trim()).
            filter(image => image.length > 0);
        
        server.listen(port, (error) => {
            if (error) {
                console.error(`Error listening on port ${port}:`, error);
                process.exit(1);
            }
            console.log(`Server started, listening on ${port}`)
        });
        
        process.on('SIGINT', () => {
            metrics.stopCollecting();
            server.close(() => {
                console.log(`Server stopped`);
                process.exit(0);
            })
        });
        
        return metrics.startCollecting({ labels, labelMappings, prefix, sampleSize, ignoreImages });
    }).
    then(undefined, error => {
        console.error('Error running container stats exporter:', error);
        process.exit(1);
    });