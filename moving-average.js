
module.exports = movingAverage;
    
function movingAverage(n) {

    if (!(n > 1)) {
        return {
            clear: (key) => { },
            add: (key, value) => value
        }
    }

    n = parseInt(n);

    let averages = {};

    return { add, clear };

    function add(key, value) {
        let entry = averages[key] || (averages[key] = { next: 0, values: new Array(n) });
        entry.values[entry.next] = value;
        entry.next = (entry.next + 1) % n;
        return sum(entry.values) / count(entry.values);
    }

    function clear(key) {
        delete averages[key];
    }
}

function sum(values) {
    return values.reduce((sum, value) => sum + (isValid(value) ? value : 0), 0);
}

function count(values) {
    return values.reduce((count, value) => count + (isValid(value) ? 1 : 0), 0);
}

function isValid(value) {
    return typeof value === 'number' && !Number.isNaN(value);
}