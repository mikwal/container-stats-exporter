let http = require('http');
let fs = require('fs');

module.exports = { getMetaData, detectIfRunningOnEC2 };

async function getMetaData(path) {

    return new Promise((resolve, reject) => {
        let request = http.request({ host: '169.254.169.254', path }, response => {
            let buffer = Buffer.alloc(0);
            response.on('data', data => buffer = Buffer.concat([buffer, data]));
            response.on('end', () => resolve(buffer.toString('utf8')));
            response.on('error', reject); 
        });
        request.on('error', reject);
        request.end();
    });
}

function detectIfRunningOnEC2() {
    let uuidPath = '/sys/hypervisor/uuid';
    let ec2Prefix = 'ec2';
    return fs.existsSync(uuidPath) && fs.readFileSync(uuidPath).toString().startsWith(ec2Prefix);
}