let { register, Gauge } = require('prom-client');
let { hashObject : hashLabels } = require('prom-client/lib/util');
let docker = require('./docker');
let movingAverage = require('./moving-average');

module.exports = {
    contentType: register.contentType,
    dump,
    startCollecting,
    stopCollecting
};

let streamRequest;

function dump() {
    return register.metrics();   
}

function startCollecting(options) {

    let { labels, labelMappings, prefix, sampleSize, ignoreImages } = options;

    let labelNames = ['id', ...Object.keys(labels || {}), ...Object.keys(labelMappings || {})];

    let metrics = {
        cpu: new Gauge({
            name: (prefix || '') + 'cpu',
            labelNames,
            help: 'CPU utilization for containers'
        }), 
        memory: new Gauge({
            name: (prefix || '') + 'memory',
            labelNames,
            help: 'Memory utilization for containers'
        })
    };

    let sampler = movingAverage(sampleSize);

    let labelsFor = container => ({
        id: container.id,
        ...(labels || {}),
        ...mapLabels(container.labels, labelMappings || {})
    });

    let isIgnored = container => (
        Array.isArray(ignoreImages) &&
        ignoreImages.some(image =>
            typeof container.image !== 'string' ||
            container.image === image ||
            container.image.startsWith(image + ':')));
    
    let resetMetrics = (container, reason) => {
        console.log(`Metrics reset, ${reason}: ${container.id} ${container.image}`);
        for (let name of Object.keys(metrics)) {
            let labelsHash = hashLabels(labelsFor(container));
            delete metrics[name].hashMap[labelsHash]; // HACK: not public API
            sampler.clear(`${name}{${labelsHash}}`);
        }
    };

    return streamRequest = docker.streamContainerStats(({ type, container, stats, error }) => {
        
        if (isIgnored(container)) {
            return;
        }

        switch (type) {

            case 'container-added':
                resetMetrics(container, 'container added');
                break;
            
            case 'container-removed':
                resetMetrics(container, 'container removed');
                break;
            
            case 'stats':
                let now = Date.now();
                for (let name of Object.keys(stats)) {
                    let metric = metrics[name];
                    let labels = labelsFor(container);
                    let value = stats[name];
                    if (metric && !Number.isNaN(value)) {
                        value = sampler.add(`${name}{${hashLabels(labels)}}`, value);
                        metric.set(labels, value, now);
                    }
                }
                break;
            
            case 'stats-error':
                console.warn(`Error refreshing container stats for ${container.id}.`, error);
                break;
        }
    });
}

function stopCollecting() {
    streamRequest && streamRequest.abort();
    streamRequest = undefined;
    register.clear();
}

function mapLabels(labels, labelMapping) {
    return Object.keys(labelMapping || {}).reduce((mapped, key) => {
        let i = labelMapping[key].findIndex(value => typeof labels[value] !== 'undefined');
        if (i >= 0) {
            mapped[key] = labels[labelMapping[key][i]];
        }
        return mapped;
    }, {});
}
