FROM node:10.15-alpine as build

COPY package.json yarn.lock ./app/
RUN cd app && yarn --production

FROM node:10.15-alpine

COPY --from=build /app /app
COPY *.js /app/

EXPOSE 9100
STOPSIGNAL SIGINT
ENTRYPOINT ["node",  "/app/server.js"]