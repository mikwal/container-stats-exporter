let http = require('http');

let socketPath = '/var/run/docker.sock';
let apiVersion = 'v1.18';

module.exports = {
    apiRequest: makeAbortable(apiRequest),
    streamContainerStats: makeAbortable(streamContainerStats)
};


async function streamContainerStats({ onabort }, callback) {

    let dockerRequest = makeAbortable(apiRequest);
 
    let now = Date.now();
    let runningContainers = {};
    let isAborted = false;

    onabort(() => isAborted = true);

    for (let { Id, Image, Labels } of await dockerRequest('containers/json')) {
    
        let container = { id: Id, image: Image, labels: Labels };
    
        runningContainers[container.id] = container;
        callback({ type: 'container-added', container });
        streamStats(container);
    }

    if (isAborted) {
        return;
    }

    let since = ((now - 500) / 1000).toFixed(0);
    let filters = JSON.stringify({ event: ['start', 'die'] });
    let eventsRequest = dockerRequest(`events?since=${since}&filters=${filters}`, handleEvent);

    onabort(() => {
        eventsRequest.abort();
        for (let id of Object.keys(runningContainers)) {
            runningContainers[id].statsRequest.abort();
        }
    });

    await eventsRequest;

    function handleEvent(event) {
    
        let { from: Image, Type, Action, Actor: { ID, Attributes } } = event;
    
        console.log(`Docker event: ${Action} ${Type} ${ID}`);

        if (Type === 'container') {
    
            let container = { id: ID, image: Image, labels: Attributes };
    
            if (Action === 'start') {
                if (!runningContainers[container.id]) {
                    runningContainers[container.id] = container;
                    streamStats(container);
                    callback({ type: 'container-added', container });
                }
                return;
            }
            if (Action === 'die') {
                if (container = runningContainers[container.id]) {
                    delete runningContainers[container.id];
                    container.statsRequest.abort();
                    callback({ type: 'container-removed', container });
                }
                return;
            }
        }

        return true;
    }

    async function streamStats(container) {

        try {
            await (container.statsRequest = dockerRequest(`containers/${container.id}/stats?stream=true`, handleStats));
        }
        catch (error) {
            callback({ type: 'stats-error', container, error });
        }

        function handleStats(stats) {

            if (!runningContainers[container.id]) {
                return;
            }

            let cpuDelta = stats.cpu_stats.cpu_usage.total_usage - stats.precpu_stats.cpu_usage.total_usage;
            let systemDelta = stats.cpu_stats.system_cpu_usage - stats.precpu_stats.system_cpu_usage;
        
            let cpu = cpuDelta / systemDelta * 100;
            let memory = stats.memory_stats.usage / stats.memory_stats.limit * 100;
        
            callback({ type: 'stats', container, stats: { cpu, memory } });
        }
    }
}

function apiRequest({ abort, onabort }, path, streamCallback) {
    
    let agent = new http.Agent();

    return new Promise((resolve, reject) => { 

        console.log(`Docker API request started: /${path}`);

        request = http.request({ socketPath, agent, path: `/${apiVersion}/${path}` }, response => {
        
            switch (response.statusCode) {
                case 200:
                    let buffer = Buffer.alloc(0), endOfStreamedChunk;
                    response.on('data', data => {

                        // Handle packets without newline / 0x0a (generated for /events in Docker v20+)
                        if (buffer.length === 0 && data.readInt8(0) === 0x7b && data.readInt8(data.length - 1) === 0x7d) {
                            try {
                                JSON.parse(data);
                                data = Buffer.concat([data, Buffer.from('\n')]);
                            }
                            catch (e) {
                            }
                        }

                        buffer = Buffer.concat([buffer, data]);
                        if (streamCallback && (endOfStreamedChunk = buffer.indexOf(0x0a)) >= 0) {
                            let chunk = buffer.slice(0, endOfStreamedChunk).toString();
                            
                            try {
                                chunk = JSON.parse(chunk);
                            }
                            catch (error) {
                                reject(new Error(`Error parsing streamed chunk from docker API: ${error.message}`));
                                abort();
                                return;
                            }
                            
                            try {
                                streamCallback(chunk);
                            }
                            catch (error) {
                                reject(error);
                                abort();
                                return;
                            }

                            buffer = buffer.slice(endOfStreamedChunk + 1);
                        }
                    });

                    response.on('end', () => {
                        console.log(`Docker API request ended: /${path}`);
                        if (streamCallback) {
                            resolve();
                        }
                        try {
                            resolve(JSON.parse(buffer.toString()));
                        }
                        catch (error) {
                            reject(new Error(`Error parsing response from docker API: ${error.message}`));
                        }
                    });
                    response.on('error', error => {
                        reject(new Error(`Error reading response from docker API: ${error.message}`));
                    });
                    break;
                
                default:
                    reject(new Error(`Unexpected status code from docker API: ${response.statusCode}`));
                    break;
            }
        });

        onabort(() => {
            console.log(`Docker API request aborted: /${path}`);
            agent.destroy();
        });

        request.end();
    });
}

function makeAbortable(f) {
    return (...args) => {
        let callbacks = [];
        let onabort = callback => callbacks.push(callback);
        let abort = () => callbacks.forEach(callback => callback());
        return Object.assign(
            new Promise((resolve, reject) => f({ abort, onabort }, ...args).then(resolve, reject)),
            { abort }
        );
    };
}
