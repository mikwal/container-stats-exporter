# Container stats exporter

Exports docker stats as prometheus metrics, designed for Amazon ECS.

Available from: https://hub.docker.com/r/mikwal/container-stats-exporter

## Usage

Needs to have the host `/var/run/docker.sock` mounted in the container.  
It listens on port `9100` for `GET /metrics` requests by default.  

## Configuration

| Environment variable                | Default Value             |
| ------------------------------------| --------------------------|
| `STATS_EXPORTER_PORT`               | `9100`                    |
| `STATS_EXPORTER_LABEL_MAPPINGS`     |  (see below)              |
| `STATS_EXPORTER_METRIC_PREFIX`      | `container_`              |
| `STATS_EXPORTER_SAMPLE_SIZE`        | `10`                      |
| `STATS_EXPORTER_IGNORE_IMAGES`      | `amazon/amazon-ecs-agent` |
| `STATS_EXPORTER_HEALTH_CHECK_IMAGE` | `amazon/amazon-ecs-agent` |


### Container/docker label to metric/prometheus label mapping

Default value:
```
cluster=com.amazonaws.ecs.cluster;
task=com.amazonaws.ecs.task-definition-family,com.docker.compose.project;
container=com.amazonaws.ecs.container-name,com.docker.compose.service
```